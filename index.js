// import thu vien
const express= require('express');
const path = require('path');
const app = new express();

const port=8000;

app.use(express.static(__dirname + "/views"));

app.get("/", (req,res) =>{
    console.log(__dirname);
    res.sendFile(path.join(__dirname+ "/views/index.html"));
})

app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})